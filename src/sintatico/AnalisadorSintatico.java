
package sintatico;


import java.util.Map;
import java.util.Stack;
import parsingTable.ParsingTable;
/**
 *
 * @author joao
 */
public class AnalisadorSintatico {
   private final Map<String, Integer> linguagem = ParsingTable.LINGUAGEM;
    private final Map<String, String> gramatica = ParsingTable.GRAMATICA;
    
    public void analisar(Stack<String> tokens) {
        
        Stack<String> a = tokens; 
        Stack<Integer> x = new Stack<>();
        
        x.push(52);
        while(!a.isEmpty()) {
            // verifica se X Ã© terminal
            System.out.println(x.peek()+"="+(getCodigo(a.peek())));
            if(x.peek()<52){
                // verifica se X = a
                if(x.peek().equals(getCodigo(a.peek()))){
                    // retira o X do topo da pilha
                    x.pop();
                    // retira o a da entrada
                    a.pop();
                }else{
                    System.out.println("erro");
                }
            }else{
                String deriva = gramatica.get(x.peek()+","+getCodigo(a.peek())); // gera a key 
                Integer[] parsing = ParsingTable.geraDadosCruzamentoTabParsingToken(deriva); // envia a key e deriva o value recebido
                x.pop();
                // se parsing for null e porque nao foi encontrado do value com a key enviada
                if(parsing != null){
                    colocaNaPilha(parsing,x); // coloca o parsing na pilha x
                }
            }
        }
        // se for true é porque todos os dados foram eliminados
        if(a.size()==0){
            System.out.println("Dados eliminados: ");
        }else{
            System.out.println("Erro ao realizar a leitura");
        }
        
        leituraStack(a);
    }    
    private String[] getGramatica(Integer X, String a){
        String[] parsing = gramatica.get(X+","+getCodigo(a)).split("@");
        return parsing;
    }
    private void leituraStack(Stack<String> tokens) {
        for (int i = 0; i < tokens.size(); i++) {
            System.out.println(tokens.get(i));
        }
    }

    private Integer getCodigo(String token) {
       if(linguagem.get(token)!=null){
            return linguagem.get(token);
       }else{
           return getIdentificadorOuInteiro(token);
       }
    }
    /**
     * Adiciona um array em uma pilha
     * @param parsing Array que vai ser adicionado na pilha
     * @param pilhaSeq pilha que vai receber
     */
    private void colocaNaPilha(Integer[] parsing, Stack pilhaSeq) {
          for (int j = parsing.length -1; j >= 0; j--) {
                    System.out.println(parsing[j]+" = "+linguagem.get(parsing[j]));
                    pilhaSeq.push(parsing[j]);
          }
    }
    
    /**
     Este metodo verifica se um token não é um IDENTIFICADOR ou um INTEIRO
     */
    private Integer getIdentificadorOuInteiro(String token) {
        //verifica se Ã© um identificador ou inteiro
        char[] cList = token.toCharArray();
        boolean identificador = true;
        for(char c : cList) {
            if(Character.getType(c) != Character.UPPERCASE_LETTER) {
                identificador = true;
            }
        }

        if(identificador) {
            return linguagem.get("IDENTIFICADOR");
        } 
        return linguagem.get("INTEIRO");
    }
}
