/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Stack;
import lexico.AnalisadorLexico;
import sintatico.AnalisadorSintatico;

public class Main {
    private String programa = "insira o código a ser analisado";
    public void iniciar() {
        
        Stack<String> tokens = new AnalisadorLexico().gerarTokens(programa);
        
        new AnalisadorSintatico().analisar(tokens);
    }

    public static void main(String args[]) {
        new Main().iniciar();
    }
}
