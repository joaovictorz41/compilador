package lexico;


import java.util.Stack;

public class AnalisadorLexico {
    
    public Stack<String> gerarTokens(String programa){
         String[] tokens = programa.split(" ");
        Stack<String> pilhaTokens = new Stack<>();
        
        for (int i = tokens.length-1; i >= 0; i--) {
            if(!tokens[i].isEmpty()){
                pilhaTokens.push(tokens[i]);
            }
        }
        return pilhaTokens;
    }
    
}
